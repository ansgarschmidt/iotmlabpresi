FROM node:latest

RUN git clone https://github.com/hakimel/reveal.js.git
WORKDIR reveal.js
RUN npm install
COPY index.html    index.html
COPY satnogs.html  satnogs.html
COPY pic           pic
EXPOSE 8000

CMD npm start